#include <iostream>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <utility>
#include <vector>
#include <thread>
#include <deque>

const int TIMEOUT = 1;
const int TIMEOUT_RECHARGING = 5;

enum CDirection {
    UP, DOWN, LEFT, RIGHT, UNKNOWN
};

//----------------------------------------------------------------------------------------------------------------------

class timeoutException : public std::exception {
    const char *what() const noexcept override {
        return "CONNECTION TIMEOUT";
    }
};

class loginFailedException : public std::exception {
    const char *what() const noexcept override {
        return "LOGIN FAILED";
    }
};

class logicErrorException : public std::exception {
    const char *what() const noexcept override {
        return "LOGIC ERROR";
    }
};

class syntaxErrorException : public std::exception {
    const char *what() const noexcept override {
        return "SYNTAX ERROR";
    }
};

class keyOutOfRangeException : public std::exception {
    const char *what() const noexcept override {
        return "KEY OUT OF RANGE";
    }
};

class messageFoundException : public std::exception {
    const char *what() const noexcept override {
        return "MESSAGE FOUND";
    }
};

//----------------------------------------------------------------------------------------------------------------------

class CPosition {
public:
    CPosition()
            : x(0), y(0) {}

    CPosition(int x, int y)
            : x(x),
              y(y) {}

    bool operator==(CPosition a) const {
        return x == a.x && y == a.y;
    }

    CDirection findDirection(CPosition oldPosition) const {
        if (oldPosition.x == x) {
            if (oldPosition.y > y)
                return DOWN;
            if (oldPosition.y < y)
                return UP;
        }
        if (oldPosition.y == y) {
            if (oldPosition.x > x)
                return LEFT;
            if (oldPosition.x < x)
                return RIGHT;
        }
        return UNKNOWN;
    }

    int getX() const {
        return x;
    }

    int getY() const {
        return y;
    }

private:
    int x;
    int y;
};

//----------------------------------------------------------------------------------------------------------------------
class CMessageService {
public:
    explicit CMessageService(int socket)
            : socketFD(socket) {}

    void sendString(const std::string &message) const {
        send(socketFD, message.c_str(), message.length(), 0);
    }

    static bool strToInt(const std::string &str, int &result) {
        for (auto &i : str) {
            if (i == str.front() && (i == '-' || i == '+'))
                continue;
            if (!isdigit(i))
                return false;
        }
        result = atoi(str.c_str());
        return true;
    }

    static bool isMessageComplete(const std::string &str) {
        std::string substr = "\a\b";
        int pos = str.find(substr, 0);
        if (pos != std::string::npos) {
            return true;
        }
        return false;
    }

    void parseBufferIntoQueue(std::string &buffer) {
        while (true) {

            std::string delimiter = "\a\b";
            int pos = buffer.find(delimiter, 0);
            if (pos == std::string::npos) {
                if (!buffer.empty())
                    commandsQueue.push_back(buffer);
                return;
            }
            commandsQueue.push_back(buffer.substr(0, pos + delimiter.length()));
            buffer.erase(0, pos + delimiter.length());
        }
    }

    void startRecharging() {
        std::string res{};
        std::string mess = "FULL POWER\a\b";
        int maxLen = mess.length();
        const int bufferSize = 1024;
        char str[bufferSize];
        struct timeval timeout;
        fd_set sockets;

        if (!commandsQueue.empty()) {
            std::string command = commandsQueue.front();

            if (isMessageComplete(command)) {
                commandsQueue.pop_front();
                if (command != mess)
                    throw logicErrorException();
                return;
            }

            res.append(command);
            commandsQueue.pop_front();
        }

        timeout.tv_sec = TIMEOUT_RECHARGING;
        timeout.tv_usec = 0;
        FD_ZERO(&sockets);
        FD_SET(socketFD, &sockets);
        while (true) {

            select(socketFD + 1, &sockets, NULL, NULL, &timeout);

            if (!FD_ISSET(socketFD, &sockets))
                throw timeoutException();

            int bytesRead = recv(socketFD, str, bufferSize - 1, 0);
            for (int i = 0; i < bytesRead; ++i)
                res.push_back(str[i]);

            parseBufferIntoQueue(res);

            std::string command = commandsQueue.front();

            if (isMessageComplete(command)) {
                commandsQueue.pop_front();
                if (command != mess)
                    throw logicErrorException();
                return;
            }

            if (command.length() > maxLen)
                throw syntaxErrorException();

            if (!commandsQueue.empty())
                commandsQueue.pop_front();
        }
    }


    std::string receive(int maxLen) {
        std::string res{};
        const int bufferSize = 1024;
        char str[bufferSize];
        struct timeval timeout;
        fd_set sockets;

        while (true) {

            if (!commandsQueue.empty()) {
                std::string command = commandsQueue.front();

                if (isMessageComplete(command)) {
                    commandsQueue.pop_front();

                    if (command == "RECHARGING\a\b") {
                        startRecharging();
                        continue;
                    }

                    if (command.length() > maxLen)
                        throw syntaxErrorException();

                    command.pop_back();
                    command.pop_back();
                    return command;
                }

                res.append(command);
                commandsQueue.pop_front();
            }

            while (true) {
                timeout.tv_sec = TIMEOUT;
                timeout.tv_usec = 0;
                FD_ZERO(&sockets);
                FD_SET(socketFD, &sockets);
                select(socketFD + 1, &sockets, NULL, NULL, &timeout);

                if (!FD_ISSET(socketFD, &sockets))
                    throw timeoutException();

                int bytesRead = recv(socketFD, str, bufferSize - 1, 0);
                for (int i = 0; i < bytesRead; ++i)
                    res.push_back(str[i]);

                parseBufferIntoQueue(res);

                std::string command = commandsQueue.front();

                if (isMessageComplete(command)) {
                    commandsQueue.pop_front();

                    if (command == "RECHARGING\a\b") {
                        startRecharging();
                        break;
                    }

                    if (command.length() > maxLen)
                        throw syntaxErrorException();

                    command.pop_back();
                    command.pop_back();
                    return command;
                }

                if (command.length() >= maxLen)
                    throw syntaxErrorException();

                if (!commandsQueue.empty())
                    commandsQueue.pop_front();
            }
        }
    }

private:
    const int socketFD;
    std::deque<std::string> commandsQueue;
};

//----------------------------------------------------------------------------------------------------------------------

class CMovementService {
public:
    CMovementService() = default;

    void setMessageService(std::shared_ptr<CMessageService> mess) {
        messageService = std::move(mess);
    }


    static CPosition cordsFromString(std::string str) {
        std::string delimiter = " ";
        str = str.erase(0, str.find(delimiter) + delimiter.length());

        std::string xStr = str.substr(0, str.find(delimiter));
        str = str.erase(0, str.find(delimiter) + delimiter.length());

        std::string yStr = str;

        int x = 0;
        int y = 0;
        if (!CMessageService::strToInt(xStr, x) || !CMessageService::strToInt(yStr, y))
            throw syntaxErrorException();

        return {x, y};
    }

    CPosition move() {
        CPosition oldPos = position;;
        messageService->sendString(SERVER_MOVE);
        auto result = cordsFromString(messageService->receive(12));

        if (result == CPosition(0, 0))
            throw messageFoundException();

        if (result == oldPos)
            overcomeObstacle();

        position = result;
        return position;
    }

    void findPosition() {
        messageService->sendString(SERVER_MOVE);
        auto pos1 = cordsFromString(messageService->receive(12));
        if (pos1 == CPosition(0, 0))
            throw messageFoundException();

        messageService->sendString(SERVER_MOVE);
        CPosition pos2 = cordsFromString(messageService->receive(12));
        if (pos2 == CPosition(0, 0))
            throw messageFoundException();

        if (pos1 == pos2) {
            messageService->sendString(SERVER_TURN_LEFT);
            pos1 = cordsFromString(messageService->receive(12));
            messageService->sendString(SERVER_MOVE);
            pos2 = cordsFromString(messageService->receive(12));
        }

        position = pos2;
        direction = pos2.findDirection(pos1);
    }

    void turnLeft() {
        messageService->sendString(SERVER_TURN_LEFT);
        switch (direction) {
            case LEFT:
                direction = DOWN;
                break;
            case RIGHT:
                direction = UP;
                break;
            case UP:
                direction = LEFT;
                break;
            case DOWN:
                direction = RIGHT;
                break;
            case UNKNOWN:
                break;
        }
        position = cordsFromString(messageService->receive(12));
    };

    void turnRight() {
        messageService->sendString(SERVER_TURN_RIGHT);
        switch (direction) {
            case LEFT:
                direction = UP;
                break;
            case RIGHT:
                direction = DOWN;
                break;
            case UP:
                direction = RIGHT;
                break;
            case DOWN:
                direction = LEFT;
                break;
            case UNKNOWN:
                break;
        }
        position = cordsFromString(messageService->receive(12));
    };

    void setDirectionLeft() {
        switch (direction) {
            case LEFT:
                return;
            case RIGHT:
                turnRight();
                turnRight();
                break;
            case UP:
                turnLeft();
                break;
            case DOWN:
                turnRight();
                break;
            case UNKNOWN:
                //TODO findDirection -> setDirectionLeft
                break;
        }
    }

    void setDirectionRight() {
        switch (direction) {
            case RIGHT:
                return;
            case LEFT:
                turnRight();
                turnRight();
                break;
            case UP:
                turnRight();
                break;
            case DOWN:
                turnLeft();
                break;
            case UNKNOWN:
                //TODO findDirection -> setDirectionRight
                break;
        }
    }

    void setDirectionDown() {
        switch (direction) {
            case DOWN:
                return;
            case UP:
                turnRight();
                turnRight();
                break;
            case LEFT:
                turnLeft();
                break;
            case RIGHT:
                turnRight();
                break;
            case UNKNOWN:
                //TODO findDirection -> setDirectionDown
                break;
        }
    }

    void setDirectionUp() {
        switch (direction) {
            case UP:
                return;
            case DOWN:
                turnRight();
                turnRight();
                break;
            case RIGHT:
                turnLeft();
                break;
            case LEFT:
                turnRight();
                break;
            case UNKNOWN:
                //TODO findDirection -> setDirectionUp
                break;
        }
    }

    void overcomeObstacleWhole() {
        turnRight();
        move();
        turnLeft();
        move();
        move();
        turnLeft();
        move();
        turnRight();
    }

    void overcomeObstacleRight() {
        turnRight();
        move();
        turnLeft();
    }

    void overcomeObstacleLeft() {
        turnLeft();
        move();
        turnRight();
    }

    void overcomeObstacle() {
        switch (direction) {
            case UP:
                if (position.getX() > 0) {
                    overcomeObstacleLeft();
                    break;
                }
                if (position.getX() < 0) {
                    overcomeObstacleRight();
                    break;
                } else {
                    overcomeObstacleWhole();
                    break;
                }
            case DOWN:
                if (position.getX() > 0) {
                    overcomeObstacleRight();
                    break;
                }
                if (position.getX() < 0) {
                    overcomeObstacleLeft();
                    break;
                } else {
                    overcomeObstacleWhole();
                    break;
                }
            case LEFT:
                if (position.getY() > 0) {
                    overcomeObstacleLeft();
                    break;
                }
                if (position.getY() < 0) {
                    overcomeObstacleRight();
                    break;
                } else {
                    overcomeObstacleWhole();
                    break;
                }
            case RIGHT:
                if (position.getY() > 0) {
                    overcomeObstacleRight();
                    break;
                }
                if (position.getY() < 0) {
                    overcomeObstacleLeft();
                    break;
                } else {
                    overcomeObstacleWhole();
                    break;
                }
            case UNKNOWN:
                break;
        }
    }

    void moveXtoZero() {
        if (position.getX() > 0)
            setDirectionLeft();
        else if (position.getX() < 0)
            setDirectionRight();
        else
            return;

        move();
    }

    void moveYtoZero() {
        if (position.getY() > 0)
            setDirectionDown();
        else if (position.getY() < 0)
            setDirectionUp();
        else
            return;

        move();
    }

    void moveToCenter() {
        while (position.getX() != 0)
            moveXtoZero();

        while (position.getY() != 0)
            moveYtoZero();
    }

    void start() {
        findPosition();
        moveToCenter();
    }

private:
    std::shared_ptr<CMessageService> messageService;
    CPosition position;
    CDirection direction = UNKNOWN;
    const std::string SERVER_MOVE = "102 MOVE\a\b";
    const std::string SERVER_TURN_LEFT = "103 TURN LEFT\a\b";
    const std::string SERVER_TURN_RIGHT = "104 TURN RIGHT\a\b";
};

//----------------------------------------------------------------------------------------------------------------------

class CLoginService {
public:
    CLoginService() = default;

    void setMessageService(std::shared_ptr<CMessageService> service) {
        messageService = std::move(service);
    }

    int keyRequest() {
        messageService->sendString(SERVER_KEY_REQUEST);
        std::string keyStr = messageService->receive(5);

        int key = atoi(keyStr.c_str());

        for (auto &i : keyStr) {
            if (i == keyStr.front() && (i == '-' || i == '+'))
                continue;
            if (!isdigit(i))
                throw syntaxErrorException();
        }

        if (key < 0 || key > 4)
            throw keyOutOfRangeException();
        return key;
    }

    int login() {
        std::string username = messageService->receive(20);

        int key = keyRequest();

        int usernameHash = hashUsername(username);
        int hash = (usernameHash + keys[key].first) % 65536;
        messageService->sendString(std::to_string(hash).append("\a\b"));

        std::string clientHashStr = messageService->receive(7);

        int clientHash = 0;
        if (!CMessageService::strToInt(clientHashStr, clientHash))
            throw syntaxErrorException();

        hash = (usernameHash + keys[key].second) % 65536;

        if (clientHash != hash)
            throw (loginFailedException());

        messageService->sendString(SERVER_OK);
        return 0;
    }

    static int hashUsername(const std::string &username) {
        int sum = 0;
        for (auto &i : username)
            sum += i;
        return (sum * 1000) % 65536;
    }

private:
    std::shared_ptr<CMessageService> messageService;
    const std::string SERVER_KEY_REQUEST = "107 KEY REQUEST\a\b";
    const std::string SERVER_OK = "200 OK\a\b";
    const std::vector<std::pair<int, int> > keys = {std::make_pair(23019, 32037), std::make_pair(32037, 29295),
                                                    std::make_pair(18789, 13603), std::make_pair(16443, 29533),
                                                    std::make_pair(18189, 21952)};
};
//----------------------------------------------------------------------------------------------------------------------

class CConnection {
public:
    explicit CConnection(int socket)
            : socketFD(socket) {
        auto messService = std::make_shared<CMessageService>(socket);
        messageService = messService;
        loginService.setMessageService(messService);
        movementService.setMessageService(messService);
    }

    void startConnectionThread() {
        connectionThread = std::thread(&CConnection::connectionFnc, this);
    }

    void connectionFnc() {
        try {
            loginService.login();
            movementService.start();
        } catch (keyOutOfRangeException &ex) {
            messageService->sendString(SERVER_KEY_OUT_OF_RANGE_ERROR);
            close(socketFD);
        } catch (loginFailedException &ex) {
            messageService->sendString(SERVER_LOGIN_FAILED);
            close(socketFD);
        } catch (syntaxErrorException &ex) {
            messageService->sendString(SERVER_SYNTAX_ERROR);
            close(socketFD);
        } catch (timeoutException &ex) {
            std::cout << "CLIENT TIMEOUTED" << std::endl;
            close(socketFD);
        } catch (logicErrorException &ex) {
            std::cout << "CLIENT LOGIC ERROR" << std::endl;
            messageService->sendString(SERVER_LOGIC_ERROR);
            close(socketFD);
        } catch (messageFoundException &ex) {
            try {
                messageService->sendString(SERVER_PICK_UP);
                messageService->receive(100);
                messageService->sendString(SERVER_LOGOUT);
                close(socketFD);
            } catch (syntaxErrorException &ex) {
                messageService->sendString(SERVER_SYNTAX_ERROR);
                close(socketFD);
            } catch (timeoutException &ex) {
                close(socketFD);
            } catch (logicErrorException &ex) {
                messageService->sendString(SERVER_LOGIC_ERROR);
                close(socketFD);
            }
        }
    }

private:
    int socketFD;
    std::thread connectionThread;
    std::shared_ptr<CMessageService> messageService;
    CLoginService loginService;
    CMovementService movementService;
    const std::string SERVER_PICK_UP = "105 GET MESSAGE\a\b";
    const std::string SERVER_LOGOUT = "106 LOGOUT\a\b";
    const std::string SERVER_LOGIN_FAILED = "300 LOGIN FAILED\a\b";
    const std::string SERVER_SYNTAX_ERROR = "301 SYNTAX ERROR\a\b";
    const std::string SERVER_LOGIC_ERROR = "302 LOGIC ERROR\a\b";
    const std::string SERVER_KEY_OUT_OF_RANGE_ERROR = "303 KEY OUT OF RANGE\a\b";
};
//----------------------------------------------------------------------------------------------------------------------

class CServer {
public:
    CServer(int port, int maxClients) {
        this->port = port;
        this->maxClients = maxClients;
    }

    void setup() {
        serverSocket = socket(AF_INET, SOCK_STREAM, 0);
        struct sockaddr_in servaddr;
        bzero(&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        servaddr.sin_port = htons(port);
        bind(serverSocket, (struct sockaddr *) &servaddr, sizeof(servaddr));
        listen(serverSocket, maxClients);
    }

    void start() {
        struct sockaddr_in theirAddr;
        socklen_t addrSize;
        while (true) {
            int newFD = accept(serverSocket, (struct sockaddr *) &theirAddr, &addrSize);
            auto connection = std::make_shared<CConnection>(newFD);
            connections.push_back(connection);
            connection.get()->startConnectionThread();
        }
    }

private:
    int maxClients;
    int port;
    int serverSocket = -1;
    std::deque<std::shared_ptr<CConnection>> connections;
};
//----------------------------------------------------------------------------------------------------------------------

int main(int argc, char **argv) {
    if (argc < 2) {
        std::cerr << "Chybi server port" << std::endl;
        return -1;
    }

    CServer server = CServer(atoi(argv[1]), 10);
    server.setup();
    server.start();
    return 0;
}
